package main

import (
	"github.com/astaxie/beego"
	_ "gitee.com/phpdi/ant-api/routers"
	_ "gitee.com/phpdi/ant-api/services"
)

func main() {
	beego.Run()
}
