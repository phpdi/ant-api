package code

import (
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

const (
	ControllerDir = "./controllers/" //控制器目录
	TablePrefix   = "t_"              //表前缀

	PermTableName = "t_perm" //权限表名称

	LocalDatableName = "goadmin" //本地数据库名
	RemoteDatableName = "sdrms" //远地数据库名


	//数据库连接信息
	LocalDatabase  = "root:root@tcp(localhost)/" + LocalDatableName + "?charset=utf8"
	RemoteDatabase = "root:root@tcp(localhost)/" + RemoteDatableName + "?charset=utf8"

	UpdatePermSql = "UPDATE `" + LocalDatableName + "`.`" + PermTableName + "` SET `name`='%s',`perm_tree_id`='%d' WHERE `id`='%d';\n"               //更新sql
	InsertPermSql = "INSERT INTO `" + LocalDatableName + "`.`" + PermTableName + "` (`name`, `perm_url`, `perm_tree_id`) VALUES ('%s', '%s', %d);\n" //插入sql

	SyncUpdateSql = "UPDATE `" + RemoteDatableName + "`.`" + PermTableName + "` SET `name`='%s',`permtree_id`='%d' WHERE `id`='%d';\n"
	SyncInsertSql = "INSERT INTO `" + RemoteDatableName + "`.`" + PermTableName + "` (`id`,`name`, `perm_url`, `perm_tree_id`) VALUES (%d,'%s', '%s', %d);\n"

	LocalSqlFile  = "./local-permhelp.sql"  //输出sql文件位置
	RemoteSqlFile = "./remote-permhelp.sql" //远程sql文件位置
)

//权限表结构
type Perm struct {
	Id         uint32
	Name       string
	PermUrl    string
	PermTreeId uint32 //用于树形结构展示
	Sort       uint32 //排序字段
}

func init() {

	if err := orm.RegisterDataBase("default", "mysql", LocalDatabase, 30); err != nil {
		panic(err)
	}

	if err := orm.RegisterDataBase("dst", "mysql", RemoteDatabase, 30); err != nil {
		panic(err)
	}

	orm.RegisterModelWithPrefix(TablePrefix, new(Perm))

}
