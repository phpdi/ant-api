package main

import (
	"flag"
	"fmt"
	"gitee.com/phpdi/ant-api/devutil/permhelp/code"
)

var (
	help bool  //帮助包
	local bool //自动生成本地数据库权限sql
	printLocalDiff bool //打印本地数据库与控制器文件权限差异
	updateTable bool //生成本地数据库权限sql,同时更新本地数据库
	remote bool //自动生成远程数据权限sql

	localPermHelp  *code.LocalPermHelp  //本地权限辅助对象

)

func init() {
	localPermHelp=code.NewLocalPermHelp("base.go")


	flag.BoolVar(&help,"h",false,"帮助包")
	flag.BoolVar(&local,"l",true,"自动生成本地权限表sql文件")
	flag.BoolVar(&updateTable,"u",false,"自动更新本地权限表数据")
	flag.BoolVar(&remote,"r",false,"自动生成远程数据库权限表sql")
	flag.BoolVar(&printLocalDiff,"p",false,"打印本地数据库与控制器文件权限数据差异")

}

func main() {

	flag.Parse()

	if help {
		flag.VisitAll(func(i *flag.Flag) {
			fmt.Printf("-%s   %s 默认:%s\n",i.Name,i.Usage,i.DefValue)
		})

		return

	}

	//只生成本地sql,不更新数据
	if local {
		if err:=localPermHelp.CreateSql(code.LocalSqlFile);err!= nil {
			fmt.Println("main.local",err)
		}
	}

	//打印本地数据库与控制器文件权限差异
	if printLocalDiff {
		localPermHelp.PrintDiff()
	}

	//生成本地数据,同时更新本地表
	if updateTable {
		if err:=localPermHelp.UpdateTable();err!= nil {
			fmt.Println("main.updateTable",err)
		}
	}

	//对比本地数据和远程数据，生成操作远程数据的sql
	if remote {
		if err:=code.NewRemotePermHelp().CreateSql(code.RemoteSqlFile);err!= nil {
			fmt.Println("main.remote",err)
		}

	}

}
