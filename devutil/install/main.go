package main

import (
	"flag"
	"fmt"
	"gitee.com/phpdi/ant-api/devutil/install/code"
)

var (
	help bool  //帮助包

	newPackageName string //新的包名
)

func init()  {
	flag.BoolVar(&help,"h",false,"帮助包")
	flag.StringVar(&newPackageName,"n","","新的包名")

}

func main()  {
	flag.Parse()


	if help {
		flag.VisitAll(func(i *flag.Flag) {
			fmt.Printf("-%s   %s 默认:%s\n",i.Name,i.Usage,i.DefValue)
		})

		return

	}

	if newPackageName== "" {
		fmt.Println("请设置新包名 n")
	}else{
		code.NewInstall(newPackageName).Start()
	}



}

