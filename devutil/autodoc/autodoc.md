---

####

请求地址：

请求方式：POST

####请求参数
| 参数   | 类型  | 必填 | 说明 |
| :---:   | :---: | :---: | :---: |
| Pid | int | 是 |  机房id |
| Nid | int | 是 |  网吧id |
| Machines | []string |  |  Machines |
| MachineKeyEnable | bool |  |  MachineKeyEnable |
| MachineKey | string |  |  MachineKey |
| IpEnable | bool |  |  IpEnable |
| Ip | uint32 |  |  Ip |
| MacEnable | bool |  |  MacEnable |
| Mac | string |  |  Mac |
| PageIndex | uint32 |  |  PageIndex |
| PageSize | int32 |  |  PageSize |
| SortCond | OrderCond |  |  SortCond |
| Machine | string |  |  Machine |



####备注```
OrderCond:{
    Type int 
    Field int 
}
```


####响应参数
```
{
    "Code": 0,
    "Data":{
        Clients:[]{
            Machine string 
            BootType uint32 
            Mac string 
            Ip uint32 
            SubnetMask uint32 
            Dns1 uint32 
            Dns2 uint32 
            Gateway uint32 
            BootMethod uint32 
            Mirror0 uint32 
            Mirror1 uint32 
            Mirror2 uint32 
            Mirror3 uint32 
            MemoryCache uint32 
            Resolutionx uint32 
            Resolutiony uint32 
            RefreshRate uint32 
            GameServerIp uint32 
            BootServerIp uint32 
            WriteServerIp uint32 
            Configid0 uint32 
            Configid1 uint32 
            Configid2 uint32 
            Configid3 uint32 
            Pnpoption uint32 
            Gpuscaling uint32 
            Dpiscaling uint32 
            Verticalsync uint32 
            Dynamicrange uint32 
            Disp3Dset uint32 
            Volenable uint32 
            Vol uint32 
            Micvolenable uint32 
            Micvol uint32 
            Admin uint32 
            SuperImageId uint32 
            Superconfigid uint32 
            Dhcp uint32 
            Deletewritebackfile uint32 
            Colordepth uint32 
            Offlinetime uint32 
            PhysxAcc uint32 
            DigitalVibrance uint32 
            LoudnessEq uint32 
            State uint32 
            NetSpeed uint32 
            OnlineTime uint32 
            SendSpeed uint32 
            RecvSpeed uint32 
            MaxSendSpeed uint32 
            Maxrecvspeed uint32 
            TotalSendBytes uint64 
            TotalRecvBytes uint64 
            MirrorID uint32 
            ConfigID uint32 
            RestoreID uint32 
            WriteDriver uint32 
            IpChn string 
            ConfigChn string 
            OnlineTimeChn string 
        }
        OnlineClientNum int 
        ClientNum int 
    },
    "Msg": ""
}
```


---
