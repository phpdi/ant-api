package autodoc

import (
	"encoding/json"
	"fmt"
	"gitee.com/phpdi/ant-api/utils"
	"testing"
)

//用户表
type User struct {
	Id         uint32
	UserName   string `field:"账号"`                       //用户名
	Password   string `field:"密码"json:"-"`               //密码
	Salt       string `json:"-"`                         //密码加密盐
	Name       string `field:"名称"  `                     //姓名
	Mobile     string `field:"手机号"  valid:"Mobile"`      //手机号码
	Email      string `field:"邮箱"  valid:"Email"`        //邮箱
	Memo       string `field:"备注"  valid:"MaxSize(20);"` //备注
	Enable     uint                                      //用户状态：1=启用,2=禁用
	IsSuper    uint                                      //超级用户标识：1=超级用户,其他=非超级用户
	//CreateTime time.Time                                 //创建时间
	//UpdateTime time.Time                                 //更新时间
}

//权限表
type Perm struct {
	Id         uint32
	Name       string
	PermUrl    string
	PermTreeId uint32 //用于树形结构展示
	Sort       uint32 //排序字段
}



type Item struct {
	UserName   string `field:"账号"`                       //字段
	Password   string `field:"密码"json:"-"`               //密码
	IsSuper    uint                                      //超级用户标识：1=超级用户,其他=非超级用户



	User User //对象

	Perms []Perm //数组

	Perm


}

var autoDoc *AutoDoc
func init()  {
	autoDoc=NewAutoDoc("./controllers/").
		SetMethod("POST").
		SetRequestParam([]RequestParam{{"Pid","int",true,"机房id"}})

}

func TestAutoDoc_SetRequest(t *testing.T) {
	user:=Item{}
	autoDoc.SetRequest(user)


	fmt.Println(autoDoc.getRequestParamString())
}

func TestAutoDoc_setRequestRemark(t *testing.T) {
	users:=[]User{}
	rt,_,_:=utils.GetStructTV(users)

	autoDoc.setRequestRemark(rt)


	fmt.Println(autoDoc.requestRemark)

}
func TestAutoDoc_Make(t *testing.T) {
	user:=Item{}

	//入参
	autoDoc.SetRequest(user)

	//出参
	autoDoc.SetResponse(user)

	autoDoc.Create()

}

//json方式打印结构体
func JsonPrint(obj interface{}) {
	tmp, _ := json.MarshalIndent(obj, "", "     ")
	fmt.Println(string(tmp))
}

func TestAutoDoc_SetResponse(t *testing.T) {

	res:=[]Item{}
	autoDoc.SetResponse(res)

	fmt.Println(autoDoc.responseString)

}


func TestAutoDoc(t *testing.T)  {

	req:=ClientListSearch{}
	ack:=ClientList{}
	autoDoc.SetRequest(req,"Nid").
		SetResponse(ack).
		Create()

}


//列表查询，交互结构
type ClientListSearch struct {
	Nid     int `field:"网吧id"`
	Machine string

	GetClientsReq
}

// 获取所有客户机信息
type GetClientsReq struct {
	Machines             []string   `protobuf:"bytes,1,rep,name=Machines,proto3" json:"Machines,omitempty"`
	MachineKeyEnable     bool       `protobuf:"varint,2,opt,name=MachineKeyEnable,proto3" json:"MachineKeyEnable,omitempty"`
	MachineKey           string     `protobuf:"bytes,3,opt,name=MachineKey,proto3" json:"MachineKey,omitempty"`
	IpEnable             bool       `protobuf:"varint,4,opt,name=IpEnable,proto3" json:"IpEnable,omitempty"`
	Ip                   uint32     `protobuf:"varint,5,opt,name=Ip,proto3" json:"Ip,omitempty"`
	MacEnable            bool       `protobuf:"varint,6,opt,name=MacEnable,proto3" json:"MacEnable,omitempty"`
	Mac                  string     `protobuf:"bytes,7,opt,name=Mac,proto3" json:"Mac,omitempty"`
	PageIndex            uint32     `protobuf:"varint,8,opt,name=PageIndex,proto3" json:"PageIndex,omitempty"`
	PageSize             int32      `protobuf:"varint,9,opt,name=PageSize,proto3" json:"PageSize,omitempty"`
	SortCond             *OrderCond `protobuf:"bytes,10,opt,name=SortCond,proto3" json:"SortCond,omitempty"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}


type OrderCond struct {
	Type                 int   `protobuf:"varint,1,opt,name=Type,proto3,enum=yjf_msg_client_to_web.OrderType" json:"Type,omitempty"`
	Field                int `protobuf:"varint,2,opt,name=Field,proto3,enum=yjf_msg_client_to_web.ClientField" json:"Field,omitempty"`
	XXX_NoUnkeyedLiteral struct{}    `json:"-"`
	XXX_unrecognized     []byte      `json:"-"`
	XXX_sizecache        int32       `json:"-"`
}



//客户机列表查询结果
type ClientList struct {
	Clients         []ClientListItem
	OnlineClientNum int //在线客户机数量
	ClientNum       int // cloud_diskless数据库表tPcBaseInfo中客户机记录总数
}

//客户列表单体
type ClientListItem struct {
	*ClientBaseInfo    //客户机基础信息
	*ClientDynamicInfo //客户机动态信息

	IpChn         string //ip地址
	ConfigChn     string //启动镜像配置名称
	OnlineTimeChn string //客户机在线时长
}


type ClientBaseInfo struct {
	Machine              string   `protobuf:"bytes,1,opt,name=Machine,proto3" json:"Machine,omitempty"`
	BootType             uint32   `protobuf:"varint,2,opt,name=BootType,proto3" json:"BootType,omitempty"`
	Mac                  string   `protobuf:"bytes,3,opt,name=Mac,proto3" json:"Mac,omitempty"`
	Ip                   uint32   `protobuf:"varint,4,opt,name=Ip,proto3" json:"Ip,omitempty"`
	SubnetMask           uint32   `protobuf:"varint,5,opt,name=SubnetMask,proto3" json:"SubnetMask,omitempty"`
	Dns1                 uint32   `protobuf:"varint,6,opt,name=Dns1,proto3" json:"Dns1,omitempty"`
	Dns2                 uint32   `protobuf:"varint,7,opt,name=Dns2,proto3" json:"Dns2,omitempty"`
	Gateway              uint32   `protobuf:"varint,8,opt,name=Gateway,proto3" json:"Gateway,omitempty"`
	BootMethod           uint32   `protobuf:"varint,9,opt,name=BootMethod,proto3" json:"BootMethod,omitempty"`
	Mirror0              uint32   `protobuf:"varint,10,opt,name=Mirror0,proto3" json:"Mirror0,omitempty"`
	Mirror1              uint32   `protobuf:"varint,11,opt,name=Mirror1,proto3" json:"Mirror1,omitempty"`
	Mirror2              uint32   `protobuf:"varint,12,opt,name=Mirror2,proto3" json:"Mirror2,omitempty"`
	Mirror3              uint32   `protobuf:"varint,13,opt,name=Mirror3,proto3" json:"Mirror3,omitempty"`
	MemoryCache          uint32   `protobuf:"varint,14,opt,name=MemoryCache,proto3" json:"MemoryCache,omitempty"`
	Resolutionx          uint32   `protobuf:"varint,15,opt,name=Resolutionx,proto3" json:"Resolutionx,omitempty"`
	Resolutiony          uint32   `protobuf:"varint,16,opt,name=Resolutiony,proto3" json:"Resolutiony,omitempty"`
	RefreshRate          uint32   `protobuf:"varint,17,opt,name=RefreshRate,proto3" json:"RefreshRate,omitempty"`
	GameServerIp         uint32   `protobuf:"varint,18,opt,name=GameServerIp,proto3" json:"GameServerIp,omitempty"`
	BootServerIp         uint32   `protobuf:"varint,19,opt,name=BootServerIp,proto3" json:"BootServerIp,omitempty"`
	WriteServerIp        uint32   `protobuf:"varint,20,opt,name=WriteServerIp,proto3" json:"WriteServerIp,omitempty"`
	Configid0            uint32   `protobuf:"varint,21,opt,name=Configid0,proto3" json:"Configid0,omitempty"`
	Configid1            uint32   `protobuf:"varint,22,opt,name=Configid1,proto3" json:"Configid1,omitempty"`
	Configid2            uint32   `protobuf:"varint,23,opt,name=Configid2,proto3" json:"Configid2,omitempty"`
	Configid3            uint32   `protobuf:"varint,24,opt,name=Configid3,proto3" json:"Configid3,omitempty"`
	Pnpoption            uint32   `protobuf:"varint,25,opt,name=Pnpoption,proto3" json:"Pnpoption,omitempty"`
	Gpuscaling           uint32   `protobuf:"varint,26,opt,name=Gpuscaling,proto3" json:"Gpuscaling,omitempty"`
	Dpiscaling           uint32   `protobuf:"varint,27,opt,name=Dpiscaling,proto3" json:"Dpiscaling,omitempty"`
	Verticalsync         uint32   `protobuf:"varint,28,opt,name=Verticalsync,proto3" json:"Verticalsync,omitempty"`
	Dynamicrange         uint32   `protobuf:"varint,29,opt,name=Dynamicrange,proto3" json:"Dynamicrange,omitempty"`
	Disp3Dset            uint32   `protobuf:"varint,30,opt,name=Disp3dset,proto3" json:"Disp3dset,omitempty"`
	Volenable            uint32   `protobuf:"varint,31,opt,name=Volenable,proto3" json:"Volenable,omitempty"`
	Vol                  uint32   `protobuf:"varint,32,opt,name=Vol,proto3" json:"Vol,omitempty"`
	Micvolenable         uint32   `protobuf:"varint,33,opt,name=Micvolenable,proto3" json:"Micvolenable,omitempty"`
	Micvol               uint32   `protobuf:"varint,34,opt,name=Micvol,proto3" json:"Micvol,omitempty"`
	Admin                uint32   `protobuf:"varint,35,opt,name=Admin,proto3" json:"Admin,omitempty"`
	SuperImageId         uint32   `protobuf:"varint,36,opt,name=SuperImageId,proto3" json:"SuperImageId,omitempty"`
	Superconfigid        uint32   `protobuf:"varint,37,opt,name=Superconfigid,proto3" json:"Superconfigid,omitempty"`
	Dhcp                 uint32   `protobuf:"varint,38,opt,name=Dhcp,proto3" json:"Dhcp,omitempty"`
	Deletewritebackfile  uint32   `protobuf:"varint,39,opt,name=Deletewritebackfile,proto3" json:"Deletewritebackfile,omitempty"`
	Colordepth           uint32   `protobuf:"varint,40,opt,name=Colordepth,proto3" json:"Colordepth,omitempty"`
	Offlinetime          uint32   `protobuf:"varint,41,opt,name=Offlinetime,proto3" json:"Offlinetime,omitempty"`
	PhysxAcc             uint32   `protobuf:"varint,42,opt,name=PhysxAcc,proto3" json:"PhysxAcc,omitempty"`
	DigitalVibrance      uint32   `protobuf:"varint,43,opt,name=DigitalVibrance,proto3" json:"DigitalVibrance,omitempty"`
	LoudnessEq           uint32   `protobuf:"varint,44,opt,name=LoudnessEq,proto3" json:"LoudnessEq,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

type ClientDynamicInfo struct {
	State                uint32   `protobuf:"varint,1,opt,name=State,proto3" json:"State,omitempty"`
	NetSpeed             uint32   `protobuf:"varint,2,opt,name=NetSpeed,proto3" json:"NetSpeed,omitempty"`
	OnlineTime           uint32   `protobuf:"varint,3,opt,name=OnlineTime,proto3" json:"OnlineTime,omitempty"`
	SendSpeed            uint32   `protobuf:"varint,4,opt,name=SendSpeed,proto3" json:"SendSpeed,omitempty"`
	RecvSpeed            uint32   `protobuf:"varint,5,opt,name=RecvSpeed,proto3" json:"RecvSpeed,omitempty"`
	MaxSendSpeed         uint32   `protobuf:"varint,6,opt,name=MaxSendSpeed,proto3" json:"MaxSendSpeed,omitempty"`
	Maxrecvspeed         uint32   `protobuf:"varint,7,opt,name=Maxrecvspeed,proto3" json:"Maxrecvspeed,omitempty"`
	TotalSendBytes       uint64   `protobuf:"varint,8,opt,name=TotalSendBytes,proto3" json:"TotalSendBytes,omitempty"`
	TotalRecvBytes       uint64   `protobuf:"varint,9,opt,name=TotalRecvBytes,proto3" json:"TotalRecvBytes,omitempty"`
	MirrorID             uint32   `protobuf:"varint,10,opt,name=MirrorID,proto3" json:"MirrorID,omitempty"`
	ConfigID             uint32   `protobuf:"varint,11,opt,name=ConfigID,proto3" json:"ConfigID,omitempty"`
	RestoreID            uint32   `protobuf:"varint,12,opt,name=RestoreID,proto3" json:"RestoreID,omitempty"`
	WriteDriver          uint32   `protobuf:"varint,13,opt,name=WriteDriver,proto3" json:"WriteDriver,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}