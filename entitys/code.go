package entitys

import "fmt"

const (
	CodeOK         =0
	CodeErr        =50
	CodeNoAuth    =51 //您没有执行该操作的权限
	CodeTOKENERR =52 //token错误
	CodeValidFail =53 //格式验证错误
)

var CodeMsg=map[uint32]string{
	CodeErr:"错误",
	CodeNoAuth:"您没有执行该操作的权限",
	CodeTOKENERR:"token错误",
}

//json数据输出
type JsonResult struct {
	Code uint32
	Msg string
	Data interface{}
}


type WebErr struct {
	Code        uint32 //错误码
	Msg         string //展示错误消息
	OriginalMsg string //原始的错误消息
}


//构造webErr
func NewWebErr(code uint32, originalMsg ...string) error {

	if code == CodeOK {
		return nil
	}

	err := WebErr{Code: code}

	if len(originalMsg) > 0 {
		err.Msg = originalMsg[0]
		err.OriginalMsg = originalMsg[0]
	}

	//填充msg
	if tmpMsg, ok := CodeMsg[code]; ok {
		err.Msg = tmpMsg
	}

	if err.Msg == "" {
		err.Msg = "错误未定义"
	}

	return err
}

func (this WebErr) Error() string {
	return this.Msg
}

func (this WebErr) Log() string {
	return fmt.Sprintf("Code:%d,Msg:%s,OriginalMsg：%s", this.Code, this.Msg, this.OriginalMsg)
}
