package entitys

import "github.com/dgrijalva/jwt-go"

//api接受协议类型
const (
	RequestTypeFromData = iota + 1
	RequestTypeApplicationJson
)

const(
	Enable=1 //有效
	Disable=2 //无效
)



//jwt认证
type CustomClaims struct {
	UserId      uint32
	jwt.StandardClaims
}

var (
	JwtSecretKey string //jwt验证密码
)
