package entitys

import "time"

const (
	IsSuper = 1 //超级用户标识
)

//用户表
type User struct {
	Id         uint32
	UserName   string `field:"账号"`                       //用户名
	Password   string `field:"密码"json:"-"`               //密码
	Salt       string `json:"-"`                         //密码加密盐
	Name       string `field:"名称"  `                     //姓名
	Mobile     string `field:"手机号"  valid:"Mobile"`      //手机号码
	Email      string `field:"邮箱"  valid:"Email"`        //邮箱
	Memo       string `field:"备注"  valid:"MaxSize(20);"` //备注
	Enable     uint                                      //用户状态：1=启用,2=禁用
	IsSuper    uint                                      //超级用户标识：1=超级用户,其他=非超级用户
	CreateTime time.Time                                 //创建时间
	UpdateTime time.Time                                 //更新时间
}

//权限表
type Perm struct {
	Id         uint32
	Name       string
	PermUrl    string
	PermTreeId uint32 //用于树形结构展示
	Sort       uint32 //排序字段
}

//角色表
type Role struct {
	Id         uint32
	Name       string           //角色名称
	Desc       string           //角色描述
	Enable     bool             //角色状态：1=启用,2=禁用
	CreateTime time.Time        //创建时间
	UpdateTime time.Time        //更新时间
	Perms      []Perm `orm:"-"` //角色的权限信息
}

//角色-权限中间表
type RolePerm struct {
	Id     uint32
	RoleId uint32
	PermId uint32
}

//角色-用户中间表
type RoleUser struct {
	Id     uint32
	RoleId uint32
	UserId uint32
}

//权限数结构
type PermTree struct {
	Id          uint32
	Name        string
	SubPermTree []PermTree
}
