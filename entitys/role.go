package entitys


//角色搜索参数
type RoleSearchParams struct {
	Pager
	Name string
}

//查询角色参数
type QueryRoleParam struct {
	RoleId int //角色Id
	TypeId int //用户类型
}

//添加角色、编辑角色
type SaveRole struct {
	RoleId  int    //角色id：修改时传入
	Name    string //角色名称
	TypeId  int    //角色类型
	Status  int    //状态：1=启用,2=禁用
	PermIds []int  //勾选了权限项的id
}

//禁用、启动角色
type EnableRole struct {
	RoleId int //角色id
	Status int //角色状态：1=启用,2=禁用
}

