package controllers

import (
	"fmt"
	"gitee.com/phpdi/ant-api/entitys"
	"gitee.com/phpdi/ant-api/services"
)

type LoginController struct {
	BaseController
}

//登录{perm:false}
func (this *LoginController)Login()  {
	var req entitys.Login

	this.mustPost()
	this.parseFormData(&req,"UserName","Password")

	fmt.Println(req)

	this.result(services.NewAuth().Login(req))
}