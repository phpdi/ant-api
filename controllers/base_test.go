package controllers

import (
	"encoding/json"
	"fmt"
	"gitee.com/phpdi/ant-api/entitys"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
	"net/http"
	"net/url"
)

type testResponseWriter struct{}

func (f *testResponseWriter) Header() http.Header {
	return http.Header{}
}
func (f *testResponseWriter) Write(b []byte) (int, error) {
	fmt.Println(string(b))
	return 0, nil
}
func (f *testResponseWriter) WriteHeader(n int) {}

//构造控制器
func (c *BaseController) makeCtx() *BaseController {

	c.Ctx = context.NewContext()
	c.Ctx.Request = &http.Request{URL: &url.URL{Scheme: "http", Host: "localhost", Path: "/"}}
	c.Ctx.ResponseWriter = &context.Response{ResponseWriter: &testResponseWriter{}, Started: true}

	c.Ctx.Input.Context = c.Ctx
	c.Ctx.Input.RequestBody = []byte{}

	c.Ctx.Output.Context = c.Ctx

	c.Data = map[interface{}]interface{}{}

	c.Prepare()


	return c
}

//设置认证信息
func (c *BaseController) setAuth(userId uint32) *BaseController {
	//用户信息
	c.authUser=entitys.User{Id:1}

	return c
}

//设置post请求参数
func (c *BaseController) setPost(formData interface{}) *BaseController {
	//请求方式
	c.requestType = entitys.RequestTypeApplicationJson

	//post
	c.Ctx.Request.Method = "POST"

	if formData != nil {

		c.Ctx.Input.RequestBody, _ = json.Marshal(formData)
	}

	return c

}

func (c *BaseController)setUrl(controller string,action string)*BaseController  {
	c.controllerName=controller
	c.actionName=action
	return c
}

//设置url请求参数
func (c *BaseController) setGet(querys ...string) *BaseController {
	if len(querys) > 0 && len(querys)%2 == 0 {
		for i := 0; i < len(querys); i++ {
			k := querys[i]
			v := querys[i+1]
			c.Ctx.Input.SetParam(k, v)
			i++
		}

	}

	return c
}

func recoverUserStop() {
	if err := recover(); err == beego.ErrAbort {
		return
	} else {
		panic(err)
	}
}
