package controllers

import (
	"gitee.com/phpdi/ant-api/entitys"
	"gitee.com/phpdi/ant-api/services"
)

type UserController struct {
	BaseController
}

//用户列表{perm:22}
func (this *UserController) List() {
	var req entitys.SearchUser

	this.mustPost()
	this.parseFormData(&req, "UserName")

	err := services.UserService.List(&req)

	this.result(req, err)
}

//添加用户{perm:22}
func (this *UserController) Add() {
	var req entitys.User

	this.mustPost()
	this.parseFormData(&req, "UserName")

	this.result(services.UserService.Add(services.NewTransactor(), req))
}

//编辑用户{perm:22}
func (this *UserController) Edit() {
	var req entitys.User

	this.mustPost()
	this.parseFormData(&req, "Id", "UserName")

	this.result(services.UserService.Add(services.NewTransactor(), req))
}

//获取登录用户信息{perm:22}
func (this *UserController) AuthUser() {
	this.result(this.getAuthUser())
}
