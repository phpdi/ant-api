# ant-api

#### 介绍
基于beego 扩展出来的，可用于快速开发的，api框架，重点是快速开发


此项目分为两个分支
* master分支为带rbac权限的版本
* base 分支为不带权限的基础版本


#### 扩展的功能
1.重写beego 格式校验，支持go tag 自定义函数参数 源码参考 utils/validation.go、utils/validation.go
> 扩展tag验证函数只需要两步
>>1.在utils/validfuncs.go中实现验证函数  
2.在 utils/validation.go注册你的验证函数即可

2.jwt登录认证

3.一些小工具
* 切片分页工具
* 切片多字段排序工具
* 事务工具 支持嵌套事务

4.api自动文档 自动分析代码生成md格式文档

5.rbac模块
* rbac权限代码
* 自动权限数据生成 

6.控制器单元测试

开发api,不需要使用postman 进行api调试，极大提高效率
>单元测试的时候,将conf文件软链接到controllers 和services下,这样goland就可以直接执行测试代码

7.根据数据库表结构生成services和controller的工具


8.总结

当你完成控制器代码的时候,api文档,api权限数据,全都可以自动生成



#### 安装教程

1.  此框架基于beego,以及gomod模式 ,[什么是gomod?](https://www.jianshu.com/p/8c7c56e1d07e)
2.  git clone https://gitee.com/phpdi/ant-api.git
3.  进行项目根目录
4.  ./devutil-install -n="xxxxxxx"  xxxxxxx是的包名称

#### 使用说明

ant-api框架完成了大量自动化工作,用10分钟完成8小时的工作

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

