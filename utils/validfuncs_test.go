package utils

import (
	"fmt"
	"testing"
)

func Test_AlphaDash(t *testing.T) {

	res := AlphaDash("123abc_")
	fmt.Println(res)

}

func Test_ChnDash(t *testing.T) {

	res := ChnDash("123abc-_")
	fmt.Println(res)

}

func Test_ChnAlphaNumeric(t *testing.T) {

	res := ChnAlphaNumeric("123abc陈@")
	fmt.Println(res)

}

func Test_Mobile(t *testing.T) {
	res := Mobile("19086545369")
	fmt.Println(res)

}
