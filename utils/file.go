package utils

import (
	"os"
)

//判定文件是否存在
func FileExist(filename string) bool {
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return false
	}
	return true
}

