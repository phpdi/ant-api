package services

import (
	"encoding/json"
	"fmt"
	"gitee.com/phpdi/ant-api/entitys"
	"github.com/dgrijalva/jwt-go"
	"testing"
)

//json方式打印结构体
func JsonPrint(obj interface{}) {
	tmp, _ := json.MarshalIndent(obj, "", "     ")
	fmt.Println(string(tmp))
}


func TestJwtService_CreateToken(t *testing.T) {
	jwts:=NewJwtService()
	token,err:=jwts.CreateToken(entitys.CustomClaims{
		UserId:5,
		StandardClaims:jwt.StandardClaims{},
	})
	if err != nil {
		t.Error(err)
	}

	fmt.Println(token)

	res,err:=jwts.ParseToken(token)
	if err != nil {
		t.Error(err)
	}

	JsonPrint(res)

}

func TestJwtService_ParseToken(t *testing.T) {
	jwts:=NewJwtService()

	res,err:=jwts.ParseToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjEsImV4cCI6MTU3NDkzNDEwN30.Rmz3dGe6qeGJbDinaeyg4Y5McX6Wn4IeSlgRfT7WHtc")
	if err != nil {
		t.Error(err)
	}

	JsonPrint(res)


}