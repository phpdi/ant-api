package services

import (
	"gitee.com/phpdi/ant-api/entitys"
	"gitee.com/phpdi/ant-api/utils"
	"testing"
)

func TestUserService_Add(t *testing.T) {
	_,err:=UserService.Add(NewTransactor(),entitys.User{
		UserName:utils.RandomString(5),
		Name:utils.RandomString(5),
		IsSuper:1,
		Enable:1,
	})
	if err != nil {
		t.Error(err)
	}
}

func TestUserService_Edit(t *testing.T) {
	err:=UserService.Edit(NewTransactor(),entitys.User{
		Id:1,
		Name:utils.RandomString(5),
	})

	if err != nil {
		t.Error(err)
	}

}